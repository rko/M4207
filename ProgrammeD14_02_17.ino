#include <LGPS.h> // On intègre la bibliothèque LGPS afin de pouvoir utiliser des fonctions déjà faite pour utiliser le GPS.
#include <LWiFi.h> // Ici une bibliothèque pour le wifi
#include <LBattery.h> // Bibliothèque pour gérer la batterie

#include <Wire.h> // Bibiliothèque pour l'écran
#include "rgb_lcd.h" // Same

gpsSentenceInfoStruct info;
gpsSentenceInfoStruct info2;
char buff1[256]; // Variable buff contenant la position GPS
char buff[256]; // Variable buff contenant le pourcentage de la batterie

#define WIFI_AP "LinkitOneWifi"
#define WIFI_PASSWORD ""
#define WIFI_AUTH LWIFI_OPEN  // choose from LWIFI_OPEN, LWIFI_WPA, or LWIFI_WEP.
#define LED 2 // Led Rouge ( D2 )
#define LED2 4 // Led Verte ( D4 )
#define BUZZER 6 // Buzzer ( D6 )
#define bouton 3// Bouton ( D3 ) 

int boutonEtat = 0;

// Configuration écran
rgb_lcd lcd;

const int colorR = 100;
const int colorG = 100;
const int colorB = 100;

// Configuration écran

////////////////////////////
/////coordonnnées IUT  /////
////////////////////////////
float topPosition = -21.339579;
float bottomPosition = -21.341867;
float leftPosition = 55.489062;
float rightPosition = 55.492007;



// Configuration de base pour le GPS ( NE PAS TOUCHER )

static unsigned char getComma(unsigned char num,const char *str)
{
  unsigned char i,j = 0;
  int len=strlen(str);
  for(i = 0;i < len;i ++)
  {
     if(str[i] == ',')
      j++;
     if(j == num)
      return i + 1; 
  }
  return 0; 
}

static double getDoubleNumber(const char *s)
{
  char buf[10];
  unsigned char i;
  double rev;
  
  i=getComma(1, s);
  i = i - 1;
  strncpy(buf, s, i);
  buf[i] = 0;
  rev=atof(buf);
  return rev; 
}

static double getIntNumber(const char *s)
{
  char buf[10];
  unsigned char i;
  double rev;
  
  i=getComma(1, s);
  i = i - 1;
  strncpy(buf, s, i);
  buf[i] = 0;
  rev=atoi(buf);
  return rev; 
}

void parseGPGGA(const char* GPGGAstr)
{
  
  double latitude;
  double longitude;
  int tmp, hour, minute, second, num ;
  if(GPGGAstr[0] == '$')
  {
    tmp = getComma(1, GPGGAstr);
    hour     = (GPGGAstr[tmp + 0] - '0') * 10 + (GPGGAstr[tmp + 1] - '0');
    minute   = (GPGGAstr[tmp + 2] - '0') * 10 + (GPGGAstr[tmp + 3] - '0');
    second    = (GPGGAstr[tmp + 4] - '0') * 10 + (GPGGAstr[tmp + 5] - '0');
    hour = hour +4;
    sprintf(buff1, "Heure de la Reunion %2d-%2d-%2d", hour, minute, second);
    Serial.println(buff1);
    
    tmp = getComma(2, GPGGAstr);
    latitude = getDoubleNumber(&GPGGAstr[tmp]);
    tmp = getComma(4, GPGGAstr);
    longitude = getDoubleNumber(&GPGGAstr[tmp]);
    sprintf(buff1, "latitude = %5.6f, longitude = %5.6f", latitude/100, longitude/100);
    Serial.println(buff1); 
    
    tmp = getComma(7, GPGGAstr);
    num = getIntNumber(&GPGGAstr[tmp]);    
    sprintf(buff1, "satellites number = %d", num);
    Serial.println(buff1); 
  }
  else
  {
    Serial.println("Not get data"); 
  }
}

// Configuration de base pour le GPS ( NE PAS TOUCHER )






void setup() {
  Serial.begin(115200); // On initialise le serial sur 115200 bauds
  LGPS.powerOn(); // On démarre le GPS
  LWiFi.begin(); // On démarre le wifi
  LWiFi.status();
  pinMode(LED, OUTPUT); // Led ROUGE 
    pinMode(LED2, OUTPUT); // Led Verte
    pinMode(BUZZER, OUTPUT); // Buzzer
    pinMode(bouton, INPUT); // Bouton

  Serial.println("Connexion en cours à l'AP");
  while (0 == LWiFi.connect(WIFI_AP, LWiFiLoginInfo(WIFI_AUTH, WIFI_PASSWORD)))
  {
    delay(1000);
  }


  lcd.begin(16, 2);
    
    lcd.setRGB(colorR, colorG, colorB);
    

Serial.println("LGPS Power on, and waiting ..."); 
  delay(3000);

  lcd.print("Gps en préparation");
    
  
}

void loop() {


 boutonEtat = digitalRead(bouton);

 if (boutonEtat == HIGH){
  int colorR=200;
  int colorG=200;
  int colorB=200;
 }

  


Serial.println("LGPS loop"); 
  LGPS.getData(&info);
  //Serial.println((char*)info.GPGGA); 
  parseGPGGA((const char*)info.GPGGA); 
  delay(2000);











  // Partie LOOP de la batterie //

    sprintf(buff,"battery level = %d", LBattery.level() );
    Serial.println(buff);

    lcd.print(buff);
    delay(1000);

// Si la batterie est à 100%, on allume juste la LED Verte

    if(LBattery.level()==100){
      digitalWrite(LED2, HIGH);   // On allume la LED verte
    } 

    // Si la batterie est à 99%, on allume juste la LED rouge
    
    else if(LBattery.level()<=33) {
      digitalWrite(LED, HIGH);   // On allume la LED Rouge
      digitalWrite(6, LOW);     // On allume le buzzer
      delay(200);
      digitalWrite(6, LOW); //On éteint le buzzer
      delay(600);
    } 

    // Si cest en dessous de 99%, on affiche juste tout est bon :) 
    
    else {
      Serial.print("tout est bon");
    }


// PARTIE LOOP DE LA BATTERIE //
}
